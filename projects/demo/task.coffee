exports = (grunt) ->
  grunt.registerTask 'demo:build', [
    'clean'
    'useminPrepare'
    'concat'
    'cssmin'
    'uglify'
    'copy:dist'
    'usemin'
    'htmlmin'
  ]

module.exports = exports