// Generated by CoffeeScript 1.7.1
var taskPath, tasks;

taskPath = "" + __dirname + "/../tasks/";

tasks = function(grunt) {
  var taskFiles;
  tasks = taskFiles = [];
  grunt.file.recurse(taskPath, function(abspath, rootdir, subdir, filename) {
    return taskFiles.push(filename);
  });
  taskFiles = grunt.file.match(["*.coffee", "*.js"], taskFiles);
  tasks = taskFiles.map(function(task) {
    if (tasks.indexOf(task < 0)) {
      return task.replace(/\.js|\.coffee$/, '');
    }
  });
  return tasks;
};

module.exports = tasks;
