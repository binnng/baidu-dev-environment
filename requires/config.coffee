#配置grunt通用任务
module.exports = (grunt) ->

  require('load-grunt-tasks') grunt
  require('time-grunt') grunt
  
  grunt.config.set "pkg", grunt.file.readJSON 'package.json'

  ip = require('ip').address()
  project = grunt.config.get 'currentTask'

  grunt.registerTask "#{project}:bower", [
    'copy:bower'
    'bowerInstall'
  ]

  grunt.registerTask "#{project}", "#{project}:build"

  grunt.registerTask "#{project}:serve", (target) ->

    if target is "dist"
      return grunt.task.run([
        "#{project}:build"
        "connect:dist:keepalive"
      ])

    grunt.task.run [
      "clean:server"
      "#{project}:bower"
      # "concurrent:server"
      # "autoprefixer"
      "connect:livereload"
      "watch"
    ]

  grunt.registerTask "#{project}:server", "#{project}serve"

  path:
    app: "projects/#{project}"
    dist: "projects/#{project}/dist"

  clean:
    dist:
      files: [
        dot: true
        src: [
          ".tmp"
          "<%= path.dist %>"
        ]
      ]
    server: '.tmp'

  copy:
    dist:
      files: [
        {
          expand: true
          dot: true
          cwd: "<%= path.app %>"
          dest: "<%= path.dist %>"
          src: [
            "*.{ico,png,txt}"
            ".htaccess"
            "*.html"
            "page/{,*/}*.html"
            "public/images/{,*/}*.{webp}"
            "fonts/*"
          ]
        }
        {
          expand: true
          cwd: ".tmp/images"
          dest: "<%= path.dist %>/images"
          src: ["generated/*"]
        }
      ]

    bower:
      expand: yes
      src: "bower_components/**"
      dest: "<%= path.app %>"

  useminPrepare:
    html: "<%= path.app %>/{,*/}*.html"
    options:
      dest: "<%= path.dist %>"

  usemin:
    html: ["<%= path.dist %>/{,*/}*.html"]
    css: ["<%= path.dist %>/public/{,*/}*.css"]
    options:
      assetsDirs: ["<%= path.dist %>"]

  # 下载bower包
  bower:
    install:
      options:
        targetDir: "<%= path.app %>/bower_components"
        layout: "byType"
        install: true
        verbose: false
        cleanTargetDir: false
        cleanBowerDir: false
        bowerOptions: {}

  bowerInstall:
    app:
    
      # Point to the files that should be updated when
      # you run `grunt bower-install`
      src: [
        "<%= path.app %>/{,*/}*.html" # .html support...
        # "app/views/**/*.jade" # .jade support...
        # "app/styles/main.scss" # .scss & .sass support...
        # "app/config.yml" # and .yml & .yaml support out of the box!
      ]
      
      # Optional:
      # ---------
      cwd: "<%= path.app %>"
      dependencies: true
      devDependencies: false
      exclude: []
      fileTypes: {}
      ignorePath: ""
      overrides: {}

  connect:
    options:
      port: 9000
      protocol: "http"
      
      # Change this to '0.0.0.0' to access the server from outside.
      hostname: "#{ip}"
      livereload: 35729

    livereload:
      options:
        open: true
        base: [
          "<%= path.app %>"
        ]

    test:
      options:
        port: 9001
        base: [
          "test"
          "<%= path.app %>"
        ]

    dist:
      options:
        open: true
        base: "<%= path.dist %>"

  htmlmin:
    dist:
      options:
        collapseWhitespace: true
        collapseBooleanAttributes: true
        removeCommentsFromCDATA: true
        removeOptionalTags: true

      files: [
        expand: true
        cwd: "<%= path.dist %>"
        src: [
          "*.html"
          "**/{,*/}*.html"
        ]
        dest: "<%= path.dist %>"
      ]

  watch:
    bower:
      files: ["<%= path.app %>/bower.json"]
      tasks: ["bowerInstall"]

    js:
      files: ["<%= path.app %>/public/{,*/}*.js"]
      tasks: [] #'newer:jshint:all'
      options:
        livereload: true

    jsTest:
      files: ["test/spec/{,*/}*.js"]
      tasks: [
        "newer:jshint:test"
        "karma"
      ]

    styles:
      files: ["<%= path.app %>/public/{,*/}*.css"]
      tasks: []
      options:
        livereload: true

    gruntfile:
      files: ["Gruntfile.js"]

    livereload:
      options:
        livereload: "<%= connect.options.livereload %>"

      files: [
        "<%= path.app %>/{,*/}*.html"
        "<%= path.app %>/public/{,*/}*.{png,jpg,jpeg,gif,webp,svg}"
      ]