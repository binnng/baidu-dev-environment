connect = require "connect"
http = require "http"
open = require "open"
ip = require "ip"

app = connect()
	#.use(connect.favicon("public/favicon.ico"))
	.use(connect.logger("dev"))
	.use(connect.static("project"))
	.use(connect.directory("project"))
	.use(connect.cookieParser())
	.use(connect.session(secret: "my secret here"))
	.use((req, res) -> res.end "Hello from Connect!\n")

http.createServer(app).listen 3000
console.log('Listening on port 3000...')

open "http://#{ip.address()}:3000"

