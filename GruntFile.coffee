exports = (grunt) ->

  grunt.config.set 'currentTask', project = grunt.cli.tasks[0].split(":")[0]

  extend = require 'extend'
  config = (require './requires/config') grunt
  
  config = extend yes, config or {}, (require "./projects/#{project}/task") grunt

  grunt.initConfig config

module.exports = exports