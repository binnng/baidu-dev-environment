百度项目开发环境
=============

目录结构如下：

* .tmp grunt生成的临时文件夹
* bower_components bower包文件
* projects 每个项目文件
* requires 依赖

所有bower包文件都在bower_components里面

```
// 给项目demo安装bower包
grunt demo:bower

// 启动项目demo
grunt demo:serve

// 启动构建后的项目demo
grunt demo:serve:dist

// 构建项目demo
grunt demo:build
```